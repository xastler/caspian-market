// // sw.js
// self.addEventListener('install', e => {
//     e.waitUntil(
//         // после установки service worker
//         // открыть новый кэш
//         caches.open('my-pwa-cache').then(cache => {
//             // добавляем все URL ресурсов, которые хотим закэшировать
//             return cache.addAll([
//                 '/?cache',
//                 '/pwa/index.html',
//                 '/image/catalog/logo.png',
//                 '/catalog/view/theme/default/stylesheet/stylesheet.css',
//                 '/catalog/view/javascript/bootstrap/css/bootstrap.min.css',
//                 '/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
//                 '/catalog/view/javascript/common.js',
//             ]);
//         })
//     );
// });
// https://deanhume.com/



const cacheName = 'OCartCache-latest';
const offlineUrl = '/pwa/index.html';

/**
 * The event listener for the service worker installation
 */
self.addEventListener('install', event => {
    console.log('installing');
    event.waitUntil(
        caches.open(cacheName)
            .then(cache => cache.addAll([
                './image/catalog/city-market-logo.png',
                './pwa/pwa_style.css',
                './catalog/view/javascript/jquery/jquery-2.1.1.min.js',
                './catalog/view/javascript/common.js',
                offlineUrl
            ]))
    );
});

/**
 * Is the current request for an HTML page?
 * @param {Object} event
 */
function isHtmlPage(event) {

    return event.request.method === 'GET' && event.request.headers.get('accept').includes('text/html');
}

/**
 * Check to see if the current connection is 2g
 * and if so, return empty svg placeholder
 */
function returnImagePlaceholder() {

    // Check if the request is for an image
    if (/\.jpg$|.png$|.gif$|.webp$/.test(event.request.url)) {

        // Return no images
        return fetch('./image/placeholder.png', {
            mode: 'no-cors'
        });
    }
}

/**
 * Fetch and cache any results as we receive them.
 */
self.addEventListener('fetch', event => {
    console.log('fetch..');
    // Check if the current request is slow 3g
    if (/\slow-2g|2g/.test(navigator.connection.effectiveType)) {
        event.respondWith(returnImagePlaceholder());
    }

// Else continue as normal
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                // Only return cache if it's not an HTML page
                if (response && !isHtmlPage(event)) {
                    return response;
                }

                return fetch(event.request).then(

                    function (response) {

                        console.log('event.request' + response);
                        // Dont cache if not a 200 response
                        if (!response || response.status !== 200) {
                            return response;
                        }

                        let responseToCache = response.clone();
                        caches.open(cacheName)
                            .then(function (cache) {
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    }
                ).catch(error => {
                    console.log('off  line');
                    console.log('event' + event.request.url);
                    // Check if the user is offline first and is trying to navigate to a web page
                    if (isHtmlPage(event)) {
                        return caches.match(offlineUrl);
                    }
                });
            })
    );
});



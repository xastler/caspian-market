$(document).ready(function () {
// Vacancy - js дял Вакансии
    var $page = $('html, body');
    $(".vacancy-items-switch").click(function () {
        if(!$(this).parent().parent().parent().hasClass('open')){
            $('.vacancy-items.open .vacancy-items-content').slideToggle();
            $('.vacancy-items.open').removeClass('open');
        }
        $(this).parent().parent().parent().toggleClass('open');
        $(this).parent().parent().next().slideToggle();
    });
    $(".vacancy-items-checked .btn").click(function () {
        const option_val = $(this).data('id');
            $("#vacancy-select").val(option_val).trigger('change');
        $page.animate({
            scrollTop: $($(this).data('href')).offset().top-80
        }, 400);
        return false;
    });
    $(".download-block-input").click(function () {
        $(this).next().click();
    });
    $('select').styler();
    $('input[type="file"]').styler();


// -------------------------------------------


    $(".mobal-menu-btn").click(function () {
        $('.main-menu').addClass('open');
    });
    $(".mobal-menu-close").click(function () {
        $('.main-menu').removeClass('open');
    });
    $('input[type="tel"], input[name="phone"], input[name="telephone"]').mask('+7 (999) 999-99-99');
    $(".popups_switch_tab").click(function () {
        if(!$(this).hasClass('open')){
            $(".popups_switch_tab").removeClass('open');
            $(this).addClass('open');
            $(".popups_tab").removeClass('open');
            $($(this).data('href')).addClass('open');
        }
    });
    $('#request_call_back-btn').on('click', function () {
        $.ajax({
            url: 'index.php?route=sendmess/send_message/send',
            type: 'post',
            dataType: 'json',
            data:  $("#request_call_back").serialize(),
            beforeSend: function () {
                if ($("textarea").is("#g-recaptcha-response")) {
                    grecaptcha.reset();
                }
                $('#request_call_back-btn').button('loading');
            },
            complete: function () {
                $('#request_call_back-btn').button('reset');
            },
            success: function (json) {
                if (json['error']) {
                    $('#request_call_back .error').remove();
                    if (json['error_name']){
                        $('#request_call_back input[name="name"]').after('<label class="error">' + json['error_name'] + '</label>');
                        $('#request_call_back input[name="name"]').addClass('has-error');
                    }
                    if (json['error_phone']){
                        $('#request_call_back input[name="phone"]').after('<label class="error">' + json['error_phone'] + '</label>');
                        $('#request_call_back input[name="phone"]').addClass('has-error');
                    }
                }
                if (json['success']) {
                    $('#request_call_back .error').remove();
                    $('#request_call_back input[name=\'name\']').val('');
                    $('#request_call_back input[name=\'phone\']').val('');

                    $('#request_call-modal').modal('hide');
                    $('body .footer').after('<div class="alert"> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                    setTimeout(function () {
                        $('.alert').detach();
                    }, 5100);
                }
            }
        });
    });
    $('#request_send_message-btn').on('click', function () {
        $.ajax({
            url: 'index.php?route=sendmess/send_message/send',
            type: 'post',
            dataType: 'json',
            data:  $("#request_send_message").serialize(),
            beforeSend: function () {
                if ($("textarea").is("#g-recaptcha-response")) {
                    grecaptcha.reset();
                }
                $('#request_send_message-btn').button('loading');
            },
            complete: function () {
                $('#request_send_message-btn').button('reset');
            },
            success: function (json) {
                if (json['error']) {
                    $('#request_send_message .error').remove();
                    if (json['error_name']){
                        $('#request_send_message input[name="name"]').after('<label class="error">' + json['error_name'] + '</label>');
                        $('#request_send_message input[name="name"]').addClass('has-error');
                    }
                    if (json['error_phone']){
                        $('#request_send_message input[name="phone"]').after('<label class="error">' + json['error_phone'] + '</label>');
                        $('#request_send_message input[name="phone"]').addClass('has-error');
                    }
                    if (json['error_email']){
                        $('#request_send_message input[name="email"]').after('<label class="error">' + json['error_email'] + '</label>');
                        $('#request_send_message input[name="email"]').addClass('has-error');
                    }
                    if (json['error_message']){
                        $('#request_send_message textarea[name="message"]').after('<label class="error">' + json['error_message'] + '</label>');
                        $('#request_send_message textarea[name="message"]').addClass('has-error');
                    }
                }
                if (json['success']) {
                    $('#request_send_message .error').remove();
                    $('#request_send_message input[name=\'name\']').val('');
                    $('#request_send_message input[name=\'phone\']').val('');
                    $('#request_send_message input[name=\'email\']').val('');
                    $('#request_send_message input[name=\'message\']').val('');

                    $('#request_call-modal').modal('hide');
                    $('body .footer').after('<div class="alert"> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                    setTimeout(function () {
                        $('.alert').detach();
                    }, 5100);
                }
            }
        });
    });

    var offset = $('header').height();
    if ($(window).scrollTop() > offset) {
        $('header').addClass('fixed');
        $('.main-menu').addClass('fixed_menu');
        $('body').addClass('fixed');
    }
    else {
        $('header').removeClass('fixed');
        $('.main-menu').removeClass('fixed_menu');
        $('body').removeClass('fixed');
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset) {
            $('header').addClass('fixed');
            $('.main-menu').addClass('fixed_menu');
            $('body').addClass('fixed');
        }
        else {
            $('header').removeClass('fixed');
            $('.main-menu').removeClass('fixed_menu');
            $('body').removeClass('fixed');
        }
    });
});
function get_popup_purchase(product_id) {
    $.magnificPopup.open({
        tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
        items: {
            src: 'index.php?route=module/popup_purchase&product_id='+product_id,
            type: 'ajax'
        }
    });

}
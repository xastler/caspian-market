<?php echo $header; ?>
    <div class="container">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <?php if ($i + 1 < count($breadcrumbs)) { ?>
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                           id="<?php echo $breadcrumb['href']; ?>"
                           href="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </a>
                        <i class="icon-right-arrow"></i>
                    <?php } else { ?>
                        <span itemscope itemtype="http://schema.org/Thing" itemprop="item"
                              id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                    <?php } ?>
                    <meta itemprop="position" content="<?php echo $i ?>"/>
                </li>
            <?php } ?>
        </ul>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="page_review <?php echo $class; ?>"><?php echo $content_top; ?>
                <form class="form-horizontal page_review-form" id="form-review">
                    <h1><?php echo $heading_title; ?></h1>
                    <?php if ($review_status) { ?>
                        <div class="page_review-list" id="review"></div>
                        <?php if ($review_guest) { ?>
                            <div class="page_review-form">
                                <h3><?php echo $text_write; ?></h3>
                                <div class="page_review-form_block">
                                    <div class="page_review-inputs">
                                        <div class="input-block">
                                            <input type="text" name="name" value="<?php /* echo $customer_name;*/ ?>"
                                                   placeholder="<?php echo $entry_name; ?>" id="input-name"
                                                   class="form-control"/>
                                        </div>
                                        <div class="input-block">
                                            <input type="text" name="email" value="<?php /* echo $customer_email;*/ ?>"
                                                   placeholder="<?php echo $entry_email; ?>" id="input-email"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="page_review-textarea">
                                        <div class="textarea-block">
                                            <textarea name="text" rows="5" id="input-review" class="form-control"
                                                  placeholder="<?php echo $entry_message; ?>"></textarea>
                                        </div>
                                        <div class="help-block"><?php echo $text_note; ?></div>
                                    </div>
                                    <?php if (isset($site_key) && $site_key) { ?>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                            </div>
                                        </div>
                                    <?php } elseif (isset($captcha) && $captcha) { ?>
                                        <?php echo $captcha; ?>
                                    <?php } ?>
                                    <button type="button" id="button-review"
                                            data-loading-text="<?php echo $text_loading; ?>"
                                            class="btn"><?php echo $button_continue; ?></button>
                                </div>
                            </div>
                        <?php } else { ?>
                            <?php echo $text_login; ?>
                        <?php } ?>
                    <?php } ?>
                </form>
                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
        <script  ><!--
            $('#review').delegate('.pagination a', 'click', function (e) {
                e.preventDefault();
                $('#review').load(this.href);
            });

            $('#review').load('<?php echo html_entity_decode($review); ?>');

            $('#button-review').on('click', function () {
                $.ajax({
                    url: '<?php echo html_entity_decode($write); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: $("#form-review").serialize(),
                    beforeSend: function () {
                        if ($("textarea").is("#g-recaptcha-response")) {
                            grecaptcha.reset();
                        }
                        $('#button-review').button('loading');
                    },
                    complete: function () {
                        $('#button-review').button('reset');
                    },
                    success: function (json) {
                        $('.alert-success, .alert-danger').remove();
                        if (json['error']) {
                            $('.page_review-form_block .error').remove();
                            $('.page_review-form_block input,.page_review-form_block textarea').removeClass('has-error');
                            if (json['error_name']){
                                $('.page_review-form_block input[name="name"]').after('<label class="error">' + json['error_name'] + '</label>');
                                $('.page_review-form_block input[name="name"]').addClass('has-error');
                            }
                            if (json['error_email']){
                                $('.page_review-form_block input[name="email"]').after('<label class="error">' + json['error_email'] + '</label>');
                                $('.page_review-form_block input[name="email"]').addClass('has-error');
                            }
                            if (json['error_text']){
                                $('.page_review-form_block textarea[name="text"]').after('<label class="error">' + json['error_text'] + '</label>');
                                $('.page_review-form_block textarea[name="text"]').addClass('has-error');
                            }
                        }
                        if (json['success']) {
                            $('body .footer').after('<div class="alert"> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                            setTimeout(function () {
                                $('.alert').detach();
                            }, 5100);
                            $('.page_review-form_block .error').remove();
                            $('input[name=\'name\']').val('');
                            $('input[name=\'email\']').val('');
                            $('textarea[name=\'text\']').val('');
                        }
                    }
                });
            });
            //--></script>
    </div>
<?php echo $footer; ?>
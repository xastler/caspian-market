<?php if ($reviews) { ?>
    <div class="row">
        <?php foreach ($reviews as $review) { ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="review-caption">
                    <i class="icon-right-quote"></i>
                    <span class="review-author"><?php echo $review['author']; ?></span>
                    <p><?php echo $review['text']; ?></p>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12"><?php echo $pagination; ?></div>
    </div>

<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>

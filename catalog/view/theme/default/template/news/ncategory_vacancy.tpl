<h1><?php echo $heading_title ?></h1>
<?php /*if ($is_author) { ?>
    <?php if ($author_image || $author_desc) { ?>
        <div class="category-info">
            <?php if ($author_image) { ?>
                <div class="image"><img src="<?php echo $author_image; ?>" alt="<?php echo $author; ?>"/></div>
            <?php } ?>
            <?php if ($author_desc) { ?>
                <?php echo $author_desc; ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } */ ?>
<?php /*if ($is_category) { ?>
    <?php if ($thumb || $description) { ?>
        <div class="category-info">
            <?php if ($thumb) { ?>
                <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"/></div>
            <?php } ?>
            <?php if ($description) { ?>
                <?php echo $description; ?>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if ($ncategories) { ?>
        <h2><?php echo $text_refine; ?></h2>
        <div class="category-list" style="border-bottom: 2px solid #eee;">
            <?php if (count($ncategories) <= 5) { ?>
                <ul>
                    <?php foreach ($ncategories as $ncategory) { ?>
                        <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <?php for ($i = 0; $i < count($ncategories);) { ?>
                    <ul>
                        <?php $j = $i + ceil(count($ncategories) / 4); ?>
                        <?php for (; $i < $j; $i++) { ?>
                            <?php if (isset($ncategories[$i])) { ?>
                                <li>
                                    <a href="<?php echo $ncategories[$i]['href']; ?>"><?php echo $ncategories[$i]['name']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } */ ?>
<?php if($description){?>
<div class="description_page">
    <?php echo $description?>
</div>
<?php } ?>
<?php if ($article) { ?>
    <div class="vacancy-list">
        <ul>
            <?php foreach ($article as $articles) { ?>
                <li class="vacancy-items">
                    <div class="vacancy-items-header">
                        <div class="vacancy-items-name"><?php echo $articles['name']; ?></div>
                        <div class="vacancy-items-h_bottom">
                            <div class="vacancy-items-city"><?php echo $articles['custom1']; ?></div>
                            <div class="vacancy-items-switch">
                                <span class="b_open"><?php echo $button_switch; ?></span>
                                <span class="b_close"><?php echo $button_switch_close; ?></span>
                                <i class="icon-down-arrow-of-angle"></i>
                            </div>
                        </div>
                    </div>
                    <div class="vacancy-items-content">
                        <div class="vacancy-items-container">
                            <div class="vacancy-items-description">
                                <?php if ($articles['description']) { ?>
                                    <?php echo $articles['description']; ?>
                                <?php } ?>
                            </div>
                            <div class="vacancy-items-checked">
                                <div class="btn" data-href=".vacancy-form"
                                     data-id="<?php echo $articles['name']; ?>, <?php echo $articles['custom1']; ?>"><?php echo $button_checked; ?></div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <div class="marg_b_30"><?php echo $pagination; ?></div>
    </div>
    <script  ><!--
        $(document).ready(function () {
            $('img.article-image').each(function (index, element) {
                var articleWidth = $(this).parent().parent().width() * 0.7;
                var imageWidth = $(this).width() + 10;
                if (imageWidth >= articleWidth) {
                    $(this).attr("align", "center");
                    $(this).parent().addClass('bigimagein');
                }
            });
            $(window).resize(function () {
                $('img.article-image').each(function (index, element) {
                    var articleWidth = $(this).parent().parent().width() * 0.7;
                    var imageWidth = $(this).width() + 10;
                    if (imageWidth >= articleWidth) {
                        $(this).attr("align", "center");
                        $(this).parent().addClass('bigimagein');
                    }
                });
            });
        });
        //--></script>
    <div class="vacancy-form">
        <h2><?php echo $vacancy_form_title ?></h2>
        <form method="post" enctype="multipart/form-data" id="vacancy-form" onsubmit="return false;">
            <fieldset>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="input-block">
                            <input type="text" name="name" value="<?php echo $name; ?>" id="vacancy-name"
                                   placeholder="<?php echo $entry_name; ?>"
                                   class="form-control <?php if ($error_name) { ?>has-error <?php } ?>"/>
                            <?php if ($error_name) { ?>
                                <div class="error"><?php echo $error_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-block">
                            <input type="text" name="email" value="<?php echo $email; ?>" id="vacancy-email"
                                   placeholder="<?php echo $entry_email; ?>"
                                   class="form-control <?php if ($error_email) { ?>has-error <?php } ?>"/>
                            <?php if ($error_email) { ?>
                                <div class="error"><?php echo $error_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-block">
                            <input type="text" name="phone" value="<?php echo $phone; ?>" id="vacancy-phone"
                                   placeholder="<?php echo $entry_phone; ?>"
                                   class="form-control <?php if ($error_phone) { ?>has-error <?php } ?>"/>
                            <?php if ($error_phone) { ?>
                                <div class="error"><?php echo $error_phone; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="textarea-block">
                            <textarea name="enquiry" rows="10" id="vacancy-enquiry"
                                      placeholder="<?php echo $entry_enquiry; ?>"
                                      class="form-control <?php if ($error_enquiry) { ?>has-error <?php } ?>"><?php echo $enquiry; ?></textarea>
                    <?php if ($error_enquiry) { ?>
                        <div class="error"><?php echo $error_enquiry; ?></div>
                    <?php } ?>
                </div>
                <div class="select-block">
                    <select name="select" id="vacancy-select">
                        <option selected value="0"><?php echo $vacancy_form_select; ?></option>
                        <?php foreach ($article as $articles) { ?>
                            <option id="<?php echo $articles['article_id']; ?>"
                                    value="<?php echo $articles['name']; ?>, <?php echo $articles['custom1']; ?>"><?php echo $articles['name']; ?>, <?php echo $articles['custom1']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form_bottom">
                    <div class="download-block">
                        <input type="file" name="file" id="vacancy-file"
                               placeholder="<?php echo $entry_file; ?>"
                               class="form-control <?php if ($error_file) { ?>has-error <?php } ?>"/>
                        <?php if ($error_file) { ?>
                            <div class="error"><?php echo $error_file; ?></div>
                        <?php } ?>
                    </div>
                    <input class="btn" type="submit" value="<?php echo $button_submit; ?>"/>
                </div>
            </fieldset>
        </form>
    </div>
    <script>
        $(document).ready(function () {
            var files;
            $('#vacancy-file').on('change', function(){
                files = this.files;
            });

            $('#vacancy-form').submit(function () {


                var data = new FormData();

                $("form#vacancy-form").serializeArray().forEach(function(field) {
                    data.append(field.name, field.value);
                });

                if( typeof files != 'undefined' ){
                    data.append( 'file', files[0] );
                }

                $.ajax({
                    url: 'index.php?route=news/ncategory/form_x',
                    type: 'post',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData : false,
                    contentType : false,
                    beforeSend: function () {
                        $('#vacancy-form .btn').button('loading');
                    },
                    complete: function () {
                        $('#vacancy-form .btn').button('reset');
                    },
                    success: function (json) {
                        $('#vacancy-form .error').remove();
                        $('#vacancy-form input').removeClass('has-error');
                        $('#vacancy-form textarea').removeClass('has-error');
                        $('#vacancy-form .jq-selectbox__select').removeClass('has-error');
                        $('#vacancy-form .download-block').removeClass('has-error');
                        if (json['name']) {
                            $('#vacancy-form #vacancy-name').after('<div class="error">' + json['name'] + '</div>');
                            $('#vacancy-form #vacancy-name').addClass('has-error');
                        }
                        if (json['email']) {
                            $('#vacancy-form #vacancy-email').after('<div class="error">' + json['email'] + '</div>');
                            $('#vacancy-form #vacancy-email').addClass('has-error');
                        }
                        if (json['phone']) {
                            $('#vacancy-form #vacancy-phone').after('<div class="error">' + json['phone'] + '</div>');
                            $('#vacancy-form #vacancy-phone').addClass('has-error');
                        }
                        if (json['enquiry']) {
                            $('#vacancy-form #vacancy-enquiry').after('<div class="error">' + json['enquiry'] + '</div>');
                            $('#vacancy-form #vacancy-enquiry').addClass('has-error');
                        }
                        if (json['select']) {
                            $('#vacancy-form #vacancy-select').after('<div class="error">' + json['select'] + '</div>');
                            $('#vacancy-form .jq-selectbox__select').addClass('has-error');
                        }
                        if (json['file']) {
                            $('#vacancy-form .download-block').after('<div class="error">' + json['file'] + '</div>');
                            $('#vacancy-form .download-block').addClass('has-error');
                        }

                        if (json['success']) {
                            $('#vacancy-form .error').remove();
                            $('#vacancy-form input').removeClass('has-error');
                            $('#vacancy-form textarea').removeClass('has-error');
                            $('#vacancy-form .jq-selectbox__select').removeClass('has-error');
                            $('#vacancy-form .download-block').removeClass('has-error');

                            $('#vacancy-form textarea,#vacancy-form input').val('');

                            $('body .footer').after('<div class="alert"> ' + json['text_message'] + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                            setTimeout(function () {
                                $('.alert').detach();
                            }, 5100);
                        }

                    }
                });
                return false;
            });
        });
    </script>
<?php } ?>
<?php if ($is_category) { ?>
    <?php if (!$ncategories && !$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } else { ?>
    <?php if (!$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } ?>
<?php if ($disqus_status) { ?>
    <script  >
        var disqus_shortname = '<?php echo $disqus_sname; ?>';
        (function () {
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
    </script>
<?php } ?>
<?php if ($fbcom_status) { ?>
    <script  >
        window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo $fbcom_appid; ?>',
                status: true,
                xfbml: true,
                version: 'v2.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php } ?>
<div class="page-special_article">
    <div class="article-content" style="background-image: url(<?php echo $thumb; ?>);">
        <h1><?php echo $heading_title; ?></h1>
        <div class="article-description">
            <?php echo $description; ?>
        </div>
    </div>
    <?php if ($products) { ?>
        <div class="row product-layout" id="page-special_product">

        </div>
    <?php } ?>
</div>
<script  >
    $('#page-special_product').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        // $('#page-special_product').fadeOut('slow');

        $('#page-special_product').load(this.href);

        // $('#page-special_product').fadeIn('slow');
    });

    $('#page-special_product').load('index.php?route=news/article/special_product&news_id=<?php echo $news_id; ?>');
</script>
<?php /* if ($is_author) { ?>
    <?php if ($author_image || $author_desc) { ?>
        <div class="category-info">
            <?php if ($author_image) { ?>
                <div class="image"><img src="<?php echo $author_image; ?>" alt="<?php echo $author; ?>"/></div>
            <?php } ?>
            <?php if ($author_desc) { ?>
                <?php echo $author_desc; ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>
<?php if ($is_category) { ?>
    <?php if ($thumb || $description) { ?>
        <div class="category-info">
            <?php if ($thumb) { ?>
                <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"/></div>
            <?php } ?>
            <?php if ($description) { ?>
                <?php echo $description; ?>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if ($ncategories) { ?>
        <h2><?php echo $text_refine; ?></h2>
        <div class="category-list" style="border-bottom: 2px solid #eee;">
            <?php if (count($ncategories) <= 5) { ?>
                <ul>
                    <?php foreach ($ncategories as $ncategory) { ?>
                        <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <?php for ($i = 0; $i < count($ncategories);) { ?>
                    <ul>
                        <?php $j = $i + ceil(count($ncategories) / 4); ?>
                        <?php for (; $i < $j; $i++) { ?>
                            <?php if (isset($ncategories[$i])) { ?>
                                <li>
                                    <a href="<?php echo $ncategories[$i]['href']; ?>"><?php echo $ncategories[$i]['name']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } */ ?>
<h1><?php echo $heading_title ?></h1>
<?php if ($article) { ?>
    <div class="special-list">
        <div class="row">
            <?php $i=1; foreach ($article as $articles) { ?>
                <div class="<?php if($i<=4){ ?>col-xs-6<?php } else { ?>col-xs-12<?php } ?>">
                    <div class="special-items <?php if($i==5){ ?>max<?php } ?>">
                        <div class="special-items-info">
                            <div class="special-items-title"><?php echo $articles['name']?></div>
                            <div class="special-items-text"><?php echo $articles['description']; ?></div>
                        </div>
                        <img class="special-items-desktop" src="<?php echo $articles['thumb']; ?>"
                             title="<?php echo $articles['name']; ?>"
                             alt="<?php echo $articles['name']; ?>"/>
                        <img class="special-items-media" src="<?php echo $articles['thumb2']; ?>"
                             title="<?php echo $articles['name']; ?>"
                             alt="<?php echo $articles['name']; ?>"/>
                        <a href="<?php echo $articles['href']; ?>" class="special-items-btn">Узнать больше</a>
                    </div>
                </div>
            <?php $i++;} ?>
            <div class="col-xs-12 marg_b_60"><?php echo $pagination; ?></div>
        </div>
    </div>
    <script  ><!--
        $(document).ready(function () {
            $('img.article-image').each(function (index, element) {
                var articleWidth = $(this).parent().parent().width() * 0.7;
                var imageWidth = $(this).width() + 10;
                if (imageWidth >= articleWidth) {
                    $(this).attr("align", "center");
                    $(this).parent().addClass('bigimagein');
                }
            });
            $(window).resize(function () {
                $('img.article-image').each(function (index, element) {
                    var articleWidth = $(this).parent().parent().width() * 0.7;
                    var imageWidth = $(this).width() + 10;
                    if (imageWidth >= articleWidth) {
                        $(this).attr("align", "center");
                        $(this).parent().addClass('bigimagein');
                    }
                });
            });
        });
        //--></script>
<?php } ?>
<?php if ($is_category) { ?>
    <?php if (!$ncategories && !$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } else { ?>
    <?php if (!$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } ?>
<?php if ($disqus_status) { ?>
    <script  >
        var disqus_shortname = '<?php echo $disqus_sname; ?>';
        (function () {
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
    </script>
<?php } ?>
<?php if ($fbcom_status) { ?>
    <script  >
        window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo $fbcom_appid; ?>',
                status: true,
                xfbml: true,
                version: 'v2.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php } ?>
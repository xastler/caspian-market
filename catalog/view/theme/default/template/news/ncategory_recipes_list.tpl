<h1><?php echo $heading_title ?></h1>

<?php if ($article) { ?>
    <div class="category_recipes">
        <div class="row">
            <?php foreach ($article as $articles) { ?>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="category_recipes-block">
                        <div class="category_recipes-img">
                            <img src="<?php echo $articles['thumb']; ?>" alt="<?php echo $articles['name']; ?>">
                        </div>
                        <div class="category_recipes-content">
                            <div class="category_recipes-name name-recipes"><?php echo $articles['name']; ?></div>
                            <div class="category_recipes-description"><?php echo $articles['description']; ?></div>
                            <div class="category_recipes-href"><a
                                        href="<?php echo $articles['href']; ?>"><?php echo $ncategory_href; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } else { ?>
    <div class="category_recipes">
        <h3 class="text-center">
            Эта категория пустая
        </h3>
    </div>
<?php } ?>

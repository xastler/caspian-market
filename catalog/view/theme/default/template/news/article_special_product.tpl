<?php if ($products) { ?>
    <?php  foreach ($products as $product) { ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="module-special-product">
            <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
        </div>
        </div>
    <?php } ?>
    <div class="col-xs-12 marg_b_60 marg_t_10"><?php echo $pagination; ?></div>
<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>



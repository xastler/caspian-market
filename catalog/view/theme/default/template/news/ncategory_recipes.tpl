<h1><?php echo $heading_title ?></h1>
<?php if ($is_category) { ?>
    <?php if ($ncategories) { ?>
        <div class="category_recipes">
        <div class="row">
            <?php foreach ($ncategories as $ncategory) { ?>
                <div class="col-xs-6 col-sm-6 col-md-6 ">
                    <div class="category_recipes-block">
                        <div class="category_recipes-img">
                            <img src="<?php echo $ncategory['thumb']; ?>" alt="<?php echo $ncategory['name']; ?>">
                        </div>
                        <div class="category_recipes-content">
                            <div class="category_recipes-name">
                                <img src="<?php echo $ncategory['thumb_icon']; ?>" alt="<?php echo $ncategory['name']; ?>">
                               <span><?php echo $ncategory['name']; ?></span>
                            </div>
                            <div class="category_recipes-description"><?php echo $ncategory['description']; ?></div>
                            <div class="category_recipes-href"><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory_href; ?></a></div>

                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        </div>
    <?php } ?>
<?php } ?>

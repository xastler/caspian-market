<div class="product-layout">
    <div class="product-thumb transition">
        <div class="image"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></div>
        <div class="caption">
            <div class="description">
                <h4><?php echo $product['name']; ?></h4>
                <?php if($product['weight_class_id']==1){$mass_ob='кг';}
                      else if($product['weight_class_id']==2) {$mass_ob='г';}
                      else if($product['weight_class_id']==7){$mass_ob='л';} ?>
                <span class="mass"><?php echo $product['weight']; ?> <?php echo $mass_ob; ?></span>
            </div>

            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span>
                        <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
        <button class="hidden" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
        <button class='btn-cart' onclick="get_popup_purchase('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
        <?php if ($product['special']) { ?>
            <span class="price-new-percent"> -<?php echo round(($product['price'] - $product['special']) / ($product['price'] / 100)) ?>% </span>
            <div class="special-date">
                <p>Акциия действует:</p>
                <span><?php echo $product['special_date_start']; ?> - <?php echo $product['special_date_end']; ?></span>
            </div>
        <?php }  ?>
    </div>
</div>
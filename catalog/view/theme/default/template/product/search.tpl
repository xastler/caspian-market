<?php echo $header; ?>
    <div class="container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <div class="block-search">
                    <input type="text" name="search" value="<?php echo $search; ?>"
                           placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control"/>
                    <button type="submit" value="<?php echo $button_search; ?>" id="button-search" class="btn-search">
                        <i class="icon-magnifying-glass"></i>
                    </button>
                </div>


                <?php if ($products) { ?>

                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="module-special-product">
                                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare)); ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="marg_b_60 marg_t_10"><?php echo $pagination; ?></div>
                <?php } else { ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
    <script  ><!--
        $('#button-search').bind('click', function () {
            url = 'index.php?route=product/search';

            var search = $('#content input[name=\'search\']').prop('value');

            if (search) {
                url += '&search=' + encodeURIComponent(search);
            }

            var category_id = $('#content select[name=\'category_id\']').prop('value');

            if (category_id > 0) {
                url += '&category_id=' + encodeURIComponent(category_id);
            }

            var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

            if (sub_category) {
                url += '&sub_category=true';
            }

            var filter_description = $('#content input[name=\'description\']:checked').prop('value');

            if (filter_description) {
                url += '&description=true';
            }

            location = url;
        });

        $('#content input[name=\'search\']').bind('keydown', function (e) {
            if (e.keyCode == 13) {
                $('#button-search').trigger('click');
            }
        });

        $('select[name=\'category_id\']').on('change', function () {
            if (this.value == '0') {
                $('input[name=\'sub_category\']').prop('disabled', true);
            } else {
                $('input[name=\'sub_category\']').prop('disabled', false);
            }
        });

        $('select[name=\'category_id\']').trigger('change');
        --></script>
<?php echo $footer; ?>
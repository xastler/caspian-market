<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if ($i + 1 < count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>"
                       href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    </a>
                    <i class="icon-right-arrow"></i>
                <?php } else { ?>
                    <span itemscope itemtype="http://schema.org/Thing" itemprop="item"
                          id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                <?php } ?>
                <meta itemprop="position" content="<?php echo $i ?>"/>
            </li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="page_contacts  <?php echo $class; ?>"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <div class="page_contacts-info">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="page_contacts-phone">
                            <div class="page_contacts-phone-icon">
                                <i class="icon-phone-call"></i>
                            </div>
                            <p>Номер телефона</p>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone); ?>"><?php echo $telephone; ?></a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="page_contacts-email">
                            <div class="page_contacts-email-icon">
                                <i class="icon-envelope"></i>
                            </div>
                            <p>Электронная почта</p>
                            <a href="mailto:<?php echo $config_email; ?>"><?php echo $config_email; ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($locations) { ?>
                <h2><?php echo $text_store; ?></h2>
                <div class="panel-group" id="accordion">
                    <?php foreach ($locations as $location) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a
                                            href="#collapse-location<?php echo $location['location_id']; ?>"
                                            class="accordion-toggle" data-toggle="collapse"
                                            data-parent="#accordion"><?php echo $location['name']; ?> <i
                                                class="fa fa-caret-down"></i></a></h4>
                            </div>
                            <div class="panel-collapse collapse"
                                 id="collapse-location<?php echo $location['location_id']; ?>">
                                <div class="panel-body">
                                    <div class="row">
                                        <?php if ($location['image']) { ?>
                                            <div class="col-sm-3"><img src="<?php echo $location['image']; ?>"
                                                                       alt="<?php echo $location['name']; ?>"
                                                                       title="<?php echo $location['name']; ?>"
                                                                       class="img-thumbnail"/></div>
                                        <?php } ?>
                                        <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br/>
                                            <address>
                                                <?php echo $location['address']; ?>
                                            </address>
                                            <?php if ($location['geocode']) { ?>
                                                <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                                                   target="_blank" class="btn btn-info"><i
                                                            class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-3"><strong><?php echo $text_telephone; ?></strong><br>
                                            <?php echo $location['telephone']; ?><br/>
                                            <br/>
                                            <?php if ($location['fax']) { ?>
                                                <strong><?php echo $text_fax; ?></strong><br>
                                                <?php echo $location['fax']; ?>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php if ($location['open']) { ?>
                                                <strong><?php echo $text_open; ?></strong><br/>
                                                <?php echo $location['open']; ?><br/>
                                                <br/>
                                            <?php } ?>
                                            <?php if ($location['comment']) { ?>
                                                <strong><?php echo $text_comment; ?></strong><br/>
                                                <?php echo $location['comment']; ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <h2><?php echo $text_contact; ?></h2>
            <form method="post" enctype="multipart/form-data" class="page_contacts-form" id="page_contacts-form">
                <fieldset>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="input-block">
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name"
                                       placeholder="<?php echo $entry_name; ?>"
                                       class="form-control <?php if ($error_name) { ?>has-error <?php } ?>"/>
                                <?php if ($error_name) { ?>
                                    <div class="error"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-block">
                                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email"
                                       placeholder="<?php echo $entry_email; ?>"
                                       class="form-control <?php if ($error_email) { ?>has-error <?php } ?>"/>
                                <?php if ($error_email) { ?>
                                    <div class="error"><?php echo $error_email; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-block">
                                <input type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone"
                                       placeholder="<?php echo $entry_phone; ?>"
                                       class="form-control <?php if ($error_phone) { ?>has-error <?php } ?>"/>
                                <?php if ($error_phone) { ?>
                                    <div class="error"><?php echo $error_phone; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="textarea-block">
                            <textarea name="enquiry" rows="10" id="input-enquiry"
                                      placeholder="<?php echo $entry_enquiry; ?>"
                                      class="form-control <?php if ($error_enquiry) { ?>has-error <?php } ?>"><?php echo $enquiry; ?></textarea>
                        <?php if ($error_enquiry) { ?>
                            <div class="error"><?php echo $error_enquiry; ?></div>
                        <?php } ?>
                    </div>
                    <?php echo $captcha; ?>
                </fieldset>
                <div class="buttons">
                        <input class="btn" type="submit" value="<?php echo $button_submit; ?>"/>
                </div>
            </form>
            <script>
                $('#page_contacts-form').submit(function () {
                    $.ajax({
                        url: 'index.php?route=information/contact/form_x',
                        type: 'post',
                        data: $('#page_contacts-form input[type=\'text\'], #page_contacts-form textarea[name=\'enquiry\']'),
                        dataType: 'json',
                        beforeSend: function () {
                            $('#page_contacts-form .btn').button('loading');
                        },
                        complete: function () {
                            $('#page_contacts-form .btn').button('reset');
                        },
                        success: function (json) {
                            $('#page_contacts-form .error').remove();
                            $('#page_contacts-form input').removeClass('has-error');
                            $('#page_contacts-form textarea').removeClass('has-error');
                            if (json['name']) {
                                $('#page_contacts-form #input-name').after('<div class="error">' + json['name'] + '</div>');
                                $('#page_contacts-form #input-name').addClass('has-error');
                            }
                            if (json['email']) {
                                $('#page_contacts-form #input-email').after('<div class="error">' + json['email'] + '</div>');
                                $('#page_contacts-form #input-email').addClass('has-error');
                            }
                            if (json['phone']) {
                                $('#page_contacts-form #input-phone').after('<div class="error">' + json['phone'] + '</div>');
                                $('#page_contacts-form #input-phone').addClass('has-error');
                            }
                            if (json['enquiry']) {
                                $('#page_contacts-form #input-enquiry').after('<div class="error">' + json['enquiry'] + '</div>');
                                $('#page_contacts-form #input-enquiry').addClass('has-error');
                            }

                            if (json['success']) {
                                $('#page_contacts-form .error').remove();
                                $('#page_contacts-form input').removeClass('has-error');
                                $('#page_contacts-form textarea').removeClass('has-error');

                                $('#page_contacts-form textarea,#page_contacts-form input').val('');

                                $('body .footer').after('<div class="alert"> ' + json['text_message'] + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                                setTimeout(function () {
                                    $('.alert').detach();
                                }, 5100);
                            }

                        }
                    });
                    return false;
                });
            </script>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

<h3><?php echo $heading_title; ?></h3>
<div class="module-special">
    <div class="prev">
        <svg xmlns="http://www.w3.org/2000/svg" width="7" height="10" viewBox="0 0 7 10">
            <path fill="#1A1A1A" fill-rule="nonzero" d="M3.169 5l3.82 3.82L5.81 10l-5-5 5-5 1.18 1.18z"/>
        </svg>
    </div>
    <div class="slider-special">
      <?php foreach ($products as $product) { ?>
        <div class="module-special-product">
          <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
        </div>
      <?php } ?>
    </div>
    <div class="next">
        <svg xmlns="http://www.w3.org/2000/svg" width="7" height="10" viewBox="0 0 7 10">
            <path fill="#1A1A1A" fill-rule="nonzero" d="M3.831 5L.011 8.82 1.19 10l5-5-5-5L.01 1.18z"/>
        </svg>
    </div>
</div>
<script  >
$('.slider-special').slick({
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    // autoplay: true,
    prevArrow: $('.module-special .prev'),
    nextArrow: $('.module-special .next'),
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            }
        },{
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        },{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
    ]
});
</script>

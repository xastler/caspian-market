<div id="recipes<?php echo $module; ?>" class="recipes-home">
    <h3>Рецепты</h3>
    <div class="items">
        <?php foreach ($banners as $banner) { ?>
            <div class="item">
                    <a href="<?php echo $banner['link']; ?>">
                        <div class="item-img">
                            <img src="<?php echo $banner['thumb']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                        </div>
                        <div class="item-content">
                            <img src="<?php echo $banner['thumb_icon']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                            <div class="item-title"><?php echo $banner['title']; ?></div>
                        </div>
                    </a>
            </div>
        <?php } ?>
    </div>
    <a class='btn-recipes' href="<?php echo $catalog_recipes; ?>"><i class="icon-dish"></i>Все рецепты</a>
</div>

<?php if($heading_title){ ?>
    <h3><?php echo $heading_title; ?></h3>
<?php } ?>
<div class="review-home">
    <div class="prev">
        <svg xmlns="http://www.w3.org/2000/svg" width="7" height="10" viewBox="0 0 7 10">
            <path fill="#1A1A1A" fill-rule="nonzero" d="M3.169 5l3.82 3.82L5.81 10l-5-5 5-5 1.18 1.18z"/>
        </svg>
    </div>
    <div class="review-slider">
        <?php foreach ($reviews as $review) { ?>
            <div class="review-caption">
                <i class="icon-right-quote"></i>
                <span class="review-author"><?php echo $review['author']; ?></span>
                <p><?php echo $review['text']; ?></p>
            </div>
        <?php } ?>
    </div>
    <?php if($button_all){ ?>
        <a class="btn-review" href="<?php echo $keyword; ?>"><i class="icon-chat"></i>Все отзывы</a>
    <?php } ?>
    <div class="next">
        <svg xmlns="http://www.w3.org/2000/svg" width="7" height="10" viewBox="0 0 7 10">
            <path fill="#1A1A1A" fill-rule="nonzero" d="M3.831 5L.011 8.82 1.19 10l5-5-5-5L.01 1.18z"/>
        </svg>
    </div>
</div>
<script>
    $('.review-slider').slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        prevArrow: $('.review-home .prev'),
        nextArrow: $('.review-home .next'),
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            },{
                breakpoint: 700,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

</script>




<?php if(0){?>
    <!--rating-->
    <div class="rating">
        <?php for ($i = 1; $i <= 5; $i++) { ?>
            <?php if ($review['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"
                                             style='color: #FC0'></i></span>
            <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"
                                             style='color: #FC0;'></i><i
                            class="fa fa-star-o fa-stack-2x"
                            style='color: #E69500;'></i></span>
            <?php } ?>
        <?php } ?>
    </div>
    <!--date-->
    <span class="review-date-added"><?php echo $review['date_added']; ?></span>
<?php } ?>
<!--<div class="row" id="newsletter">-->
<!--	<div class="col-sm-12">-->
<!--			<div class="newsletter-form">-->
<!--				<div>--><?php //echo $text_before; ?><!--</div>-->
<!--				<form id="lt_newsletter_form">-->
<!--					<div class="form-group">-->
<!--						<div class="newsletter_form-input">-->
<!--							<input type="email" required name="lt_newsletter_email" id="lt_newsletter_email" class="form-control input-lg" placeholder="--><?php //echo $entry_email; ?><!--">-->
<!--						</div>-->
<!--						<div class="input-group-btn">-->
<!--								<button type="submit" class="btn btn-newsletter btn-default"><i class="icon-rectangle-2-copy-3"></i></button>-->
<!--						</div>-->
<!--					</div>-->
<!--				</form>-->
<!--	</div>-->
<!--	</div>-->
<!--</div>-->

<div class="newsletters">
    <h2><?php echo $heading_title; ?></h2>
    <form method="post" class="newsletters-form" id="lt_newsletter_form">
        <input type="email" name="lt_newsletter_email" id="lt_newsletter_email" value="" placeholder="Введите электронную почту" class="newsletters-input"/>
        <button class="newsletters-btn" type="submit">Подписаться</button>
    </form>
</div>
<script  ><!--
		$(document).ready(function($) {
			$('#lt_newsletter_form').submit(function(){
				$.ajax({
					type: 'post',
					url: '<?php echo $action; ?>',
					data:$("#lt_newsletter_form").serialize(),
					dataType: 'json',
					beforeSend: function() {
						$('.btn-newsletter').attr('disabled', true).button('loading');
					},
					complete: function() {
						$('.btn-newsletter').attr('disabled', false).button('reset');
					},
					success: function(json) {
						$('.error').remove();
						$('#lt_newsletter_email').removeClass('has-error');

						if (json.error) {
                            $('#lt_newsletter_email').after('<label class="error">' + json.error + '</label>');
                            $('#lt_newsletter_email').addClass('has-error');
						} else {
                            $('body .footer').after('<div class="alert"> ' + json.success + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                            setTimeout(function () {
                                $('.alert').detach();
                            }, 5100);
							$('#lt_newsletter_email').val('');
                            $('.error').remove();
                            $('#lt_newsletter_email').removeClass('has-error');
						}
					}

				});
				return false;
			});
		});
	//--></script>

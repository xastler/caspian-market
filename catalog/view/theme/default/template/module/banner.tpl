<div id="banner<?php echo $module; ?>" class="baner-home">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
      <div class="item-content">
          <div class="item-title"><?php echo $banner['title']; ?></div>
          <div class="item-text"><?php echo $banner['description']; ?></div>
      </div>

        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive item-desktop" />
      <?php if($banner['image2']){?>
        <img src="<?php echo $banner['image2']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive item-mobal" />
      <?php }?>
        <a href="<?php echo $banner['link']; ?>">Узнать больше</a>
  </div>
  <?php } ?>
</div>


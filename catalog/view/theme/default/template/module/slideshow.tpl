<div id="slideshow<?php echo $module; ?>" class="hidden-xs">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
      <div class="item-title"><?php echo $banner['title']; ?></div>
      <div class="item-text"><?php echo $banner['description']; ?></div>
    <?php if ($banner['link']) { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <a href="<?php echo $banner['link']; ?>">Узнать больше</a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script  >
$('#slideshow<?php echo $module; ?>').slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true
});
</script>
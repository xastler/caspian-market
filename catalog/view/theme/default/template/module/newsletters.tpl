<script>
		function subscribe()
		{
			var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			var email = $('#txtemail').val();
            $('#txtemail').next('.error').remove();
            $('#txtemail').removeClass('has-error');
			if(email != "")
			{
				if(!emailpattern.test(email))
				{
                    $('#txtemail').after('<label class="error">E-Mail указан некорректно!</label>');
                    $('#txtemail').addClass('has-error');
					return false;
				}
				else
				{
					$.ajax({
						url: 'index.php?route=module/newsletters/news',
						type: 'post',
						data: 'email=' + $('#txtemail').val(),
						dataType: 'json',
						
									
						success: function(json) {
                            $('body .footer').after('<div class="alert"> ' + json.message + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');
                            setTimeout(function () {
                                $('.alert').detach();
                            }, 5100);

						}
						
					});
					return false;
				}
			}
			else
			{
                $('#txtemail').after('<label class="error">Введите Email</label>');
                $('#txtemail').addClass('has-error');
				$(email).focus();
				return false;
			}
			

		}
	</script>
	
<div class="newsletters">
    <h2><?php echo $heading_title; ?></h2>
    <form method="post" class="newsletters-form">
        <input type="email" name="txtemail" id="txtemail" value="" placeholder="Введите электронную почту" class="newsletters-input"/>
        <button class="newsletters-btn" type="submit" onclick="return subscribe();">Подписаться</button>
    </form>
</div>
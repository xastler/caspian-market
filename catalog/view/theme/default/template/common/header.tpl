<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php if ($lang == 'ua') {
    $lang = 'uk';
}
echo $lang; ?>">
<!--<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" href="/reject/reject.css" media="all"/>
<script src="/reject/reject.min.js"></script>
<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <?php if ($gtm) { ?>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?
                id = '+i+dl;f.parentNode.insertBefore(j,f);
            })(window, document, 'script', 'dataLayer', '<?php echo $gtm; ?>');</script>
        <!-- End Google Tag Manager -->
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"  ></script>
<!--    <script src="catalog/view/javascript/jquery.lazyloadxt.js"  ></script>-->
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"  ></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/fonts-text/fonts.css" rel="stylesheet">
    <link href="catalog/view/theme/default/fonts-icon/style.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main-css.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
    <script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"  ></script>
    <link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.js"></script>
    <script src="catalog/view/javascript/popup_purchase/jquery.magnific-popup.min.js"  ></script>
    <link href="catalog/view/javascript/popup_purchase/magnific-popup.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet" media="screen" />

    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
              media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/common.js"  ></script>
    <script src="catalog/view/javascript/main.js"  ></script>

    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>"  ></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if ($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-2 hidden-md hidden-lg">
                <div class="mobal-menu-btn">
                    <i class="icon-menu"></i>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-8">
                <div class="header-logo">
                    <div id="logo">
                        <?php if ($logo) { ?>
                            <?php if ($home == $og_url) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                                     class="img-responsive"/>
                            <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>"
                                                                    title="<?php echo $name; ?>"
                                                                    alt="<?php echo $name; ?>" class="img-responsive"/></a>
                            <?php } ?>
                        <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm hidden-xs">
                <div class="header-info">
                    <div class="header-info-a">
                        <a class="tell"
                           href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone); ?>"><?php echo $telephone; ?></a>
                        <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                    </div>
                    <div class="header-callback">
                        <button data-toggle="modal" data-target="#request_call-modal"><i class="icon-phone-call"></i>Горячая
                            линия
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-2">
                <?php echo $search; ?>
            </div>
        </div>
    </div>
</header>
<div class="main-menu">

    <div class="container">
        <div class="mobal-menu-close hidden-md hidden-lg">
            <i class="icon-cross-remove-sign "></i>
        </div>
        <div class="mobal-menu-logo hidden-md hidden-lg">
            <?php if ($logo) { ?>
                <?php if ($home == $og_url) { ?>
                    <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                         class="img-responsive"/>
                <?php } else { ?>
                    <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>"
                                                        title="<?php echo $name; ?>"
                                                        alt="<?php echo $name; ?>" class="img-responsive"/></a>
                <?php } ?>
            <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
            <?php } ?>
        </div>
        <div class="header-info  hidden-md hidden-lg">
            <div class="header-info-a">
                <a class="tell"
                   href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone); ?>"><?php echo $telephone; ?></a>
                <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
            </div>
            <div class="header-callback">
                <button data-toggle="modal" data-target="#request_call-modal"><i class="icon-phone-call"></i>Горячая
                    линия
                </button>
            </div>
        </div>
            <ul>
                <?php if ($informations) {
                    $k = 0; ?>
                    <?php foreach ($informations as $information) {
                        $k++; ?>
                        <?php if ($k == 1) { ?>
                            <li <?php if ($information['href'] == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                            </li>
                            <li <?php if ($catalog_special == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $catalog_special; ?>"><?php echo $text_special; ?></a></li>
                            <li <?php if ($news == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $news; ?>"><?php echo $text_newsletter; ?></a></li>
                        <?php } elseif ($k == 4) { ?>
                            <li <?php if ($catalog_vacancy == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $catalog_vacancy; ?>"><?php echo $text_catalog_vacancy; ?></a></li>
                            <li <?php if ($information['href'] == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                            </li>
                        <?php } else { ?>
                            <li <?php if ($information['href'] == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <li <?php if ($catalog_special == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $catalog_special; ?>"><?php echo $text_special; ?></a></li>
                    <li <?php if ($news == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $news; ?>"><?php echo $text_newsletter; ?></a></li>
                    <li <?php if ($catalog_vacancy == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $catalog_vacancy; ?>"><?php echo $text_catalog_vacancy; ?></a></li>
                <?php } ?>
                <li <?php if ($contact == $og_url) { ?>class="active"<?php } ?>><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
            </ul>
    </div>
</div>

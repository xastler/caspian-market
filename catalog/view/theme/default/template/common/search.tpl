<div id="search" class="header-search">
    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>"/>
    <button type="button"><i class="icon-magnifying-glass"></i></button>
</div>
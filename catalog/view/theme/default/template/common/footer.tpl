<footer class="footer">
    <div class="container">
        <div class="footer-content">
<!--            --><?php //echo $newsletters ?>
            <?php echo $lt_newsletters ?>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="footer-contacts">
                        <a href="mailto:<?php echo $email ;?>"><?php echo $email; ?></a>
                        <a class="tell" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>"><?php echo $telephone; ?></a>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-lg-push-4">
                    <div class="footer-callback">
                        <button data-toggle="modal" data-target="#request_call-modal"><i class="icon-phone-call"></i>Горячая линия</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-lg-pull-4">
                    <ul class="footer-social">
                        <li><a href="<?php echo $facebook; ?>" target="_blank" class="icon-facebook-logo"></a></li>
                        <li><a href="<?php echo $vk; ?>" target="_blank" class="icon-vk-social-network-logo"></a></li>
                        <li><a href="<?php echo $instagram; ?>" target="_blank" class="icon-instagram"></a></li>
                        <li><a href="<?php echo $youtube; ?>" target="_blank" class="icon-youtube"></a></li>
                    </ul>
                </div>
            </div>
            <div class="copyright"><?php echo $powered; ?></div>
        </div>
    </div>
</footer>
<div class="modal fade main_popups" id="request_call-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon-close"></i></button>
            <div class="modal-header">
                <h3 class="modal-title"><?php echo $text_call; ?></h3>
                <div class="popups_switch">
                    <div class="popups_switch_tab open" data-href="#tab_1"><?php echo $text_call_back; ?></div>
                    <div class="popups_switch_tab" data-href="#tab_2"><?php echo $text_call_message; ?></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="popups_tabs">
                    <div class="popups_tab open" id="tab_1">
                        <form class="request_call-form" id="request_call_back">
                            <div class="form-group">
                                <input type="text" name="name" id="input-name_call" placeholder="<?php echo $entry_name; ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" id="input-phone_call" placeholder="<?php echo $entry_phone; ?>" class="form-control"/>
                            </div>
                            <?php if (isset($site_key) && $site_key) { ?>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                    </div>
                                </div>
                            <?php } elseif(isset($captcha) && $captcha){ ?>
                                <?php echo $captcha; ?>
                            <?php } ?>
                            <input name="goal" value="callback_request" type="hidden">
                        </form>
                        <button type="button" id="request_call_back-btn" data-loading-text="<?php echo $text_loading; ?>" class="btn"><?php echo $text_send; ?></button>
                    </div>
                    <div class="popups_tab" id="tab_2">
                        <form class="request_call-form" id="request_send_message">
                            <div class="form-group">
                                <input type="text" name="name" id="input-name_send" placeholder="<?php echo $entry_name; ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" id="input-phone_send" placeholder="<?php echo $entry_phone; ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" id="input-email_send" placeholder="<?php echo $entry_email; ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="input-message_send" placeholder="<?php echo $entry_message; ?>" class="form-control"></textarea>
                            </div>
                            <?php if (isset($site_key) && $site_key) { ?>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                    </div>
                                </div>
                            <?php } elseif(isset($captcha) && $captcha){ ?>
                                <?php echo $captcha; ?>
                            <?php } ?>
                            <input name="goal" value="send_message" type="hidden">
                        </form>
                        <button type="button" id="request_send_message-btn" data-loading-text="<?php echo $text_loading; ?>" class="btn"><?php echo $text_send; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
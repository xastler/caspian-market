<?php echo $header; ?>
    <div class="container">
        <?php echo $column_left; ?>
            <div class="not_found"><?php echo $content_top; ?>
                <div class="not_found-img"></div>
                <h1><?php echo $text_error; ?></h1>
                <div class="buttons">
                    <a href="<?php echo $continue; ?>" class="btn">На главную</a>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?>
    </div>
<?php echo $footer; ?>
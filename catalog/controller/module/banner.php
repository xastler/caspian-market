<?php

class ControllerModuleBanner extends Controller
{
    public function index($setting)
    {
        static $module = 0;

        $this->load->model('catalog/ncategory');
        $this->load->model('catalog/news');
        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

        $data['banners'] = array();

        $ncategory_info = $this->model_catalog_ncategory->getncategory(61);

        $sdata = array(
            'filter_ncategory_id' => $ncategory_info['ncategory_id'],
            'start' => (1 - 1) * 2,
            'limit' => 2
        );
        $results = $this->model_catalog_news->getNews($sdata);

        foreach ($results as $result) {
            $href = ($ncategory_info) ? $this->url->link('news/article', 'ncat=' . 61 . '&news_id=' . $result['news_id']) : $this->url->link('news/article', 'news_id=' . $result['news_id']);
            if($result['image2']){
                $image2 = $this->model_tool_image->resize($result['image2'], 344, 260);
            }else{
                $image2 = $this->model_tool_image->resize('mask-3.png', 344, 260);
            }
            $data['banners'][] = array(
                'title' => $result['title'],
                'description' => $result['description2'],
                'link' => $href,
                'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']),
                'image2' => $image2
            );
        }

        $data['module'] = $module++;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/banner.tpl', $data);
        } else {
            return $this->load->view('default/template/module/banner.tpl', $data);
        }
    }
}
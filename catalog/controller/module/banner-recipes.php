<?php
class ControllerModuleBannerRecipes extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['banners'] = array();

//        foreach ($results as $result) {
//            if ($result['image']) {
//                $image = $this->model_tool_image->resize($result['image'], 277, 277);
//            } else {
//                $image = $this->model_tool_image->resize('placeholder.png', 277, 277);
//            }
//            if ($result['image_icon']) {
//                $image_icon =HTTP_IMAGE.''.$result['image_icon'];
//            } else {
//                $image_icon = false;
//            }
//            $data['ncategories'][] = array(
//                'name' => $result['name'],
//                'thumb' => $image,
//                'thumb_icon' => $image_icon,
//                'description' => html_entity_decode($result['description']),
//                'href' => $this->url->link('news/ncategory', 'ncat=' . $this->request->get['ncat'] . '_' . $result['ncategory_id'])
//            );
//        }
		$results = $this->model_catalog_ncategory->getncategories(62);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
                if ($result['image_icon']) {
                    $image_icon = HTTP_IMAGE.''.$result['image_icon'];
                } else {
                    $image_icon = false;
                }
				$data['banners'][] = array(
					'title' => $result['name'],
					'description' => $result['description'],
					'link'  => $this->url->link('news/ncategory', 'ncat=62_' . $result['ncategory_id']),
					'thumb' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']),
                    'thumb_icon' => $image_icon
				);
			}
		}
        $data['catalog_recipes'] = $this->url->link('news/ncategory', 'ncat=' . 62);

		$data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner-recipes.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/banner-recipes.tpl', $data);
		} else {
			return $this->load->view('default/template/module/banner-recipes.tpl', $data);
		}
	}
}
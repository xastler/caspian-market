<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = 'Copyright  &copy; %s  %s.Твой семейный магазин';

$_['entry_name']      	= 'Ваше имя';
$_['entry_phone']       = 'Телефон';
$_['entry_email']       = 'Введите электронную почту';
$_['entry_message']     = 'Сообщение';
$_['text_call']      	= 'Горячая линия';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';
$_['text_call_back']    = 'Обратный звонок';
$_['text_call_message'] = 'Напишите нам';

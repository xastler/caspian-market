<?php
// Heading
$_['heading_title'] = 'Отзывы';

$_['text_write']               = 'оставить Отзыв';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews.';
$_['text_note']                = 'Сообщение не должно содержать более 300 символов';
$_['text_success']             = 'Спасибо за ваш отзыв. Он был передан вебмастеру на утверждение.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';

// Entry
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Your Review';
$_['entry_email']              = 'Введите электронную почту';
$_['entry_message']            = 'Сообщение';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Button
$_['button_continue']          = 'Отправить';

// Error
$_['error_name']               = 'Имя должно быть от 1 до 32 символов!';
$_['error_email']              = 'Е-mail адрес введён неверно!';
$_['error_text']            = 'Комментарий должен быть от 25 до 300 символов!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';

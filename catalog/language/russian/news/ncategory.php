<?php
// Text
$_['text_refine']       = 'Subcategories';
$_['heading_title']     = 'Новости';
$_['text_error']        = 'Articles category not found!';
$_['text_empty']        = 'There are no articles here yet. Come back here soon, we will add some.';
$_['text_comments']     = 'comments on this article';
$_['text_comments_v']   = 'комментарии';
$_['button_more']       = 'Читать подробнее';
$_['text_posted_by']    = 'Posted by';
$_['text_posted_on']    = 'On';
$_['text_posted_pon']   = 'Опубликовано';
$_['text_posted_in']    = 'Опубликовано в разделе';
$_['text_updated_on']   = 'Updated on';
$_['go_to_headlines']   = 'Go to Headlines';
$_['heading_title']   = 'НОВОСТИ';
$_['text_success']   = '<p>Ваш запрос был успешно отправлен администрации магазина!</p>';
$_['button_submit']   = 'Отправить';
$_['button_switch']   = 'Развернуть';
$_['button_switch_close']   = 'Свернуть';
$_['button_checked']   = 'Отправить резюме';
$_['ncategory_href']   = 'Узнать больше';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_email']    = 'Введите электронную почту';
$_['entry_phone']    = 'Телефон';
$_['entry_enquiry']  = 'Сообщение';
$_['entry_file']  = 'Выберите файл для отправки';
$_['entry_select']  = 'Выберите файл для отправки';


// mail
$_['mail_name']     = 'Имя:';
$_['mail_email']    = 'Email:';
$_['mail_phone']    = 'Телефон:';
$_['mail_enquiry']  = 'Сообщение:';
$_['mail_file']    = 'Файл:';
$_['mail_select']  = 'Вакансия:';
$_['mail_title']  = 'Резюме';

// vacancy_form
$_['vacancy_form_title']     = 'Отправить резюме';
$_['vacancy_form_select']     = 'Выберите вакансию из списка';

// Email
$_['email_subject']  = 'Сообщение %s';

// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_email']    = 'E-Mail указан некорректно!';
$_['error_phone']    = 'Неверно введен номер телефона!';
$_['error_enquiry']  = 'Сообщение должно быть от 10 до 3000 символов!';
$_['error_file']     = 'Нужно загрузить файл';
$_['error_select']  = 'Нужно выбрать вакансию';
?>